<?php

namespace ProjectManagerApi\Repository;

use Doctrine\ORM\EntityManager;
use ProjectManagerApi\Model\Project;
use Ramsey\Uuid\UuidInterface;

class ProjectRepositoryFromDoctrine implements ProjectRepository
{
    public function __construct(private EntityManager $entityManager)
    {

    }

    public function store(Project $project): void
    {
        $this->entityManager->persist($project);
        $this->entityManager->flush();
    }

    public function getById(UuidInterface $projectId): Project
    {
        return $this
            ->entityManager
            ->getRepository(Project::class)
            ->find($projectId);
    }

    public function all(): array
    {
        return $this
            ->entityManager
            ->getRepository(Project::class)
            ->findAll();

    }
}