<?php

namespace ProjectManagerApi\Repository;

use ProjectManagerApi\Model\Project;
use Ramsey\Uuid\UuidInterface;

interface ProjectRepository
{
    public function store(Project $project): void;
    public function getById(UuidInterface $projectId): Project;
    /** @return Project[] */
    public function all(): array;
}