<?php

namespace ProjectManagerApi\Repository;

use DI\Container;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

class ProjectRepositoryFactory
{
    public static function make(ContainerInterface $container): ProjectRepository
    {
        $em = $container->get(EntityManager::class);
        return new ProjectRepositoryFromDoctrine($em);
    }

}