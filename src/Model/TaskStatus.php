<?php

namespace ProjectManagerApi\Model;

enum TaskStatus: string
{
    case ToDo = 'todo';
    case Doing = 'doing';
    case Done = 'done';


}