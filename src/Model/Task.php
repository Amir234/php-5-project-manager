<?php

namespace ProjectManagerApi\Model;
//created as anemic object to move faster

use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity, ORM\Table(name: 'project_task')]
class Task
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(type: 'uuid', unique:  true)]
        #[ORM\GeneratedValue(strategy: 'CUSTOM')]
        #[ORM\CustomIdGenerator(class: UuidGenerator::class)]
        public  UuidInterface $id,
        #[ORM\Column(name: 'project_id', type: 'uuid', unique: false)]
        public UuidInterface $project,
        #[ORM\Column(name: 'name', type: 'string', unique: false)]
        public readonly string $name,
        #[ORM\Column(name: 'status', type: 'string', unique: false, columnDefinition: TaskStatus::class)]
        public TaskStatus $status,
        #[ORM\Column(name: 'description', type: 'string', unique: true)]
        public ?string $description
    ){}
}