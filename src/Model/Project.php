<?php

namespace ProjectManagerApi\Model;


use ArrayIterator;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;
use Doctrine\ORM\Mapping as ORM;


#[ORM\Entity, ORM\Table(name: 'projects')]

class Project
{
   public function __construct(
       #[ORM\Id]
       #[ORM\Column(type: 'uuid', unique:  true)]
       #[ORM\GeneratedValue(strategy: 'CUSTOM')]
       #[ORM\CustomIdGenerator(class: UuidGenerator::class)]

       private UuidInterface $id,
       #[ORM\Column(name: 'name', type: 'string', nullable: false)]

       private string $name,
       #[ORM\Column(name: 'description', type: 'string', nullable: true)]

       private string $description,
       #[ORM\Column(name: 'created_at', type: 'date_immutable', nullable: false)]

       private DateTimeImmutable $createdAt,
       #[ORM\JoinTable(name: 'project_tasks')]
       #[ORM\JoinColumn(name: 'project_id', referencedColumnName: 'id')]
       #[ORM\OneToMany(targetEntity: Task::class, cascade: ['persist'])]
       private ?Collection $tasks = null
   ){
       $this->tasks = new ArrayCollection([]);
   }

public function id(): UuidInterface
{
    return $this->id;
}
public function name(): string
{
    return $this->name;
}
public function description(): string
{
    return $this->description;
}
public function createdAt(): DateTimeImmutable
{
    return $this->createdAt;
}

    /**
     * @return Collection<Task> */
public function tasks(): Collection
{
    return $this->tasks;
}
public function addTask(Task $task): void
{
    $this->tasks->add($task);
}

}