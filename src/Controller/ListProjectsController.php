<?php

namespace ProjectManagerApi\Controller;

use Laminas\Diactoros\Response\JsonResponse;
use ProjectManagerApi\Repository\ProjectRepository;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ListProjectsController
{
    private ProjectRepository $projectRepository;

    public function __construct(ContainerInterface $container)
    {
        $this->projectRepository = $container->get(ProjectRepository::class);
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $projects = $this->projectRepository->all();
        $arrResponse = [];
        foreach ($projects as $project) {
            $arrResponse[] = [
                'id' => $project->id()->toString(),
                'name' => $project->name(),
               // 'task' => $project->tasks()->toArray()
            ];
        }
        return new JsonResponse($arrResponse);
    }

}