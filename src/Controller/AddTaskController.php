<?php

namespace ProjectManagerApi\Controller;

use Laminas\Diactoros\Response\JsonResponse;
use ProjectManagerApi\Model\Task;
use ProjectManagerApi\Model\TaskStatus;
use ProjectManagerApi\Repository\ProjectRepository;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Ramsey\Uuid\Nonstandard\Uuid;

class AddTaskController
{
    private ProjectRepository $projectRepository;

    public function __construct(ContainerInterface $container)
    {
        $this->projectRepository = $container->get(ProjectRepository::class);
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $params = json_decode($request->getBody()->getContents(), true);

        $projectId = Uuid::fromString($args['project_id']);

        $project = $this->projectRepository->getById($projectId);

        $task = new Task(
          Uuid::uuid4(),
          $projectId,
          $params['name'],
          TaskStatus::ToDo,
          $params['description']
        );

        $project->addTask($task);
        $this->projectRepository->store($project);

        return new JsonResponse([
           'status' => 201,
           'message' => 'Task created',
           'data' => [
               'project_id' => $project->id()->toString(),
               'task_id' => $task->id->toString(),
           ]
        ], 201);

    }

}