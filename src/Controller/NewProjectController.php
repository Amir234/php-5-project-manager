<?php

namespace ProjectManagerApi\Controller;

use Laminas\Diactoros\Response\JsonResponse;
use ProjectManagerApi\Model\Project;
use ProjectManagerApi\Repository\ProjectRepository;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Ramsey\Uuid\Uuid;

class NewProjectController
{
    private ProjectRepository $projectRepository;

    public function __construct(ContainerInterface $container)
    {
        $this->projectRepository = $container->get(ProjectRepository::class);
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $params = json_decode($request->getBody()->getContents(), true);

        $project = new Project(
            Uuid::uuid4(),
            $params['name'],
            $params['description'],
            new \DateTimeImmutable()
        );
        $this->projectRepository->store();

        return new JsonResponse([
            'status' => 201,
            'message' => 'Project created',
            'data' => [
                'id' => $project->id()->toString()
            ]
        ], 201);
    }

}