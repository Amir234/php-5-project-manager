<?php

use Doctrine\ORM\EntityManager;
use ProjectManagerApi\Controller\AddTaskController;
use ProjectManagerApi\Controller\NewProjectController;
use ProjectManagerApi\Model\Task;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Ramsey\Uuid\Nonstandard\Uuid;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();

$container = require __DIR__ . '/../config/container.php';

$app->get('/', function (Request $request, Response $response, $args) use ($container) {
    $response->getBody()->write("Hello world!");
    return $response;
});

$app->get('/v1/project', new ListProjectsController($container));
$app->post('/v1/project', new NewProjectController($container));
$app->post('/v1/project/{project_id}/task', new AddTaskController($container));

$app->run();
